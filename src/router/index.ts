import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: '/Convertir',
  },
  {
    path: '/Convertir',
    component: () => import ('../views/Convertir.vue')
  },
  {
    path: '/Lista',
    component: () => import ('../views/Lista.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
