export default function numeracionMayaCefalomorfo() {
  const convertirAMayaCefalomorfo = (numeroEntrada: string) => {
      let temp = Number(numeroEntrada);
      let contador = 0; // posición del número (de abajo para arriba)
      let numeroMaya; // número que ocupa cierta posición
      let base = 20; // base de la numeración en cierta posición
      const resultado = [];
      if(temp == 0) { // convierte el 0
        resultado.unshift('assets/cefalomorfos/0_maya_head.png');
      }
      while(temp > 0) {
        if(contador == 1)
            base = 18;
        else if(contador == 2)
            base = 20;
        switch(temp % base) {
          case 0:
            numeroMaya = 'assets/cefalomorfos/0_maya_head.png';
            break;
          case 1:
            numeroMaya = 'assets/cefalomorfos/1_maya_head.png';
            break;
          case 2:
            numeroMaya = 'assets/cefalomorfos/2_maya_head.png';
            break;
          case 3:
            numeroMaya = 'assets/cefalomorfos/3_maya_head.png';
            break;
          case 4:
            numeroMaya = 'assets/cefalomorfos/4_maya_head.png';
            break;
          case 5:
            numeroMaya = 'assets/cefalomorfos/5_maya_head.png';
            break;
          case 6:
            numeroMaya = 'assets/cefalomorfos/6_maya_head.png';
            break;
          case 7:
            numeroMaya = 'assets/cefalomorfos/7_maya_head.png';
            break;
          case 8:
            numeroMaya = 'assets/cefalomorfos/8_maya_head.png';
            break;
          case 9:
            numeroMaya = 'assets/cefalomorfos/9_maya_head.png';
            break;
          case 10:
            numeroMaya = 'assets/cefalomorfos/10_maya_head.png';
            break;
          case 11:
            numeroMaya = 'assets/cefalomorfos/11_maya_head.png';
            break;
          case 12:
            numeroMaya = 'assets/cefalomorfos/12_maya_head.png';
            break;
          case 13:
            numeroMaya = 'assets/cefalomorfos/13_maya_head.png';
            break;
          case 14:
            numeroMaya = 'assets/cefalomorfos/14_maya_head.png';
            break;
          case 15:
            numeroMaya = 'assets/cefalomorfos/15_maya_head.png';
            break;
          case 16:
            numeroMaya = 'assets/cefalomorfos/16_maya_head.png';
            break;
          case 17:
            numeroMaya = 'assets/cefalomorfos/17_maya_head.png';
            break;
          case 18:
            numeroMaya = 'assets/cefalomorfos/18_maya_head.png';
            break;
          case 19:
            numeroMaya = 'assets/cefalomorfos/19_maya_head.png';
            break;
          default:
            numeroMaya = '';
        }
        resultado.unshift(numeroMaya);
        temp = Math.floor(temp / base);
        contador++;
      }
      return {cadena: resultado, esImagen: true};
  }

  return convertirAMayaCefalomorfo;
}
