export default function numeracionMaya() {
  const convertirAMaya = (numeroEntrada: string) => {
      let temp = Number(numeroEntrada);
      let numeroMaya; // número que ocupa cierta posición
      const resultado = [];
      if(temp == 0) { // convierte el 0
        resultado.unshift('𝋠');
      }
      while(temp > 0) {
        switch(temp % 20) {
          case 0:
            numeroMaya = '𝋠';
            break;
          case 1:
            numeroMaya = '𝋡';
            break;
          case 2:
            numeroMaya = '𝋢';
            break;
          case 3:
            numeroMaya = '𝋣';
            break;
          case 4:
            numeroMaya = '𝋤';
            break;
          case 5:
            numeroMaya = '𝋥';
            break;
          case 6:
            numeroMaya = '𝋦';
            break;
          case 7:
            numeroMaya = '𝋧';
            break;
          case 8:
            numeroMaya = '𝋨';
            break;
          case 9:
            numeroMaya = '𝋩';
            break;
          case 10:
            numeroMaya = '𝋪';
            break;
          case 11:
            numeroMaya = '𝋫';
            break;
          case 12:
            numeroMaya = '𝋬';
            break;
          case 13:
            numeroMaya = '𝋭';
            break;
          case 14:
            numeroMaya = '𝋮';
            break;
          case 15:
            numeroMaya = '𝋯';
            break;
          case 16:
            numeroMaya = '𝋰';
            break;
          case 17:
            numeroMaya = '𝋱';
            break;
          case 18:
            numeroMaya = '𝋲';
            break;
          case 19:
            numeroMaya = '𝋳';
            break;
          default:
            numeroMaya = '';
        }
        resultado.unshift(numeroMaya);
        temp = Math.floor(temp / 20);
      }
      return {cadena: resultado, esImagen: false};
  }

  return convertirAMaya;
}